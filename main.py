import cv2 # Bibliotek som kan lese datastrøm fra kamera
from os.path import exists
from playsound import playsound
from pyzbar.pyzbar import decode, ZBarSymbol# Bibliotek som kan lese strekkoder

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    cv2.imshow('Input', frame)
    for strekkode_objekt in decode(frame): # Loop av alle strekkoder i bildet
        strekkode = strekkode_objekt.data.decode('utf-8')
        print(strekkode_objekt)
        # Norske matvarer og bøker har som regel strekkodetype EAN13
        if strekkode_objekt.type == 'EAN13':
            lydfil = strekkode+'.wav'
            if exists(lydfil):
                playsound(lydfil) # Spill lydfil hvis den finnes
            else:
                playsound("skanner.wav") # Eller spill skannerlyd
    c = cv2.waitKey(1)
    if c == 27: # Se om noen har trykket Esc
        break

cap.release()
cv2.destroyAllWindows()