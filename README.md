# strekkode

# Prosjektets mål

- [x] Åpne webkamera
- [x] Lese strekkode (EAN-13)
- [x] Lag lyd når strekkode registreres
- [x] Lag kulyd for melkekartong
- [x] Lag hønelyd for eggekartong

# Notater

Legg til eggne lydfiler hvis du har lyst. De trenger formatet <strekkode>.wav

Ubuntu har enkel redskap for å lage lydopptak.

- Åpne et kommandovindu
- arecord filnavn.wav
- snakk inn i mikrofonen
- trykk Ctrl+C når du er ferdig
